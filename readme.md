##task 5
Znając schemat tabel "pracownik" i "dzial" napisz zapytania SQL, które zwrócą:
- Nazwiska pracowników i nazw działów, w których są zatrudnieni
- Nazwy działów i liczbę pracowników w każdym z nich
- Działów niemających żadnego pracownika
- Działów posiadających co najmniej 2 pracowników
- Działów wraz ze średnią i maksymalna pensją w dziale, posortowane rosnąco wg średniej

```
Tabela "pracownik":
-------------------
id
imie
nazwisko
pensja
dzial_id
```
```
Tabela "dzial":
---------------
id
nazwa
```
